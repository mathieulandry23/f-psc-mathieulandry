﻿using System.Collections.Generic;
using FPSC.Data.DTO;

namespace FPSC.PizzaService
{
    public interface IPizzaService
    {
        List<PizzaMenuItemDTO> GetMenu();
        IResult<PizzaOrderDTO> GetPizzaOrder(int orderId);
        IResult<PizzaOrderDTO> CreatePizzaOrder(PizzaUserOrderDTO order);
        IResult AddPizzaOrderItem(int orderId, PizzaUserOrderDTO userOrder);
    }
}