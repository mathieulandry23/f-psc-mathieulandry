﻿using AutoMapper;
using FPSC.Data;
using FPSC.Data.DTO;
using FPSC.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FPSC.PizzaService
{
    public class PizzaRioService : IPizzaService
    {
        private readonly PizzaContext _context;
        private readonly IMapper _mapper;

        public PizzaRioService(PizzaContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public List<PizzaMenuItemDTO> GetMenu()
        {
            // The use of "_mapper" let you esily convert an object to an other using properties reflection
            // You can define your own mapping method inside the "PizzaProfile" constructor
            var menu = _context.PizzaMenuItem.Include(item => item.PizzaSize)
                                             .Include(item => item.PizzaType)
                                             .Select(_mapper.Map<PizzaMenuItemDTO>);
            
            return menu.ToList();
        }

        public IResult<PizzaOrderDTO> GetPizzaOrder(int orderId)
        {
            throw new NotImplementedException();
        }

        public IResult<PizzaOrderDTO> CreatePizzaOrder(PizzaUserOrderDTO userOrder)
        {
            var newOrder = new PizzaOrder()
            {
                Date = DateTime.Now,
            };

            newOrder.PizzaOrderItems = userOrder.OrderItems.Select(x => {
                var pizza = _context.PizzaMenuItem.FirstOrDefault(p => p.PizzaMenuItemId == x.PizzaMenuItemId);
                return new PizzaOrderItem() {
                    PizzaOrder = newOrder,
                    PizzaType = pizza.PizzaType,
                    UserName = userOrder.UserName,
                    PizzaSizeId = pizza.PizzaSizeId
                };
            }).ToList();
            
            _context.PizzaOrder.Add(newOrder);

            try
            {
                _context.SaveChanges();
            }
            catch(Exception e)
            {
                //pas trop sur de comprendre pk la bd n'accepte pas PizzaSize
                var messages = new []{e.Message};

                return Result<PizzaOrderDTO>.NullResult()
                    .WithMessages(messages);
            }

            return Result.From(newOrder)
                .Map(_mapper.Map<PizzaOrderDTO>);
        }

        public IResult AddPizzaOrderItem(int orderId, PizzaUserOrderDTO userOrder)
        {
            throw new NotImplementedException();
        }
    }
}
