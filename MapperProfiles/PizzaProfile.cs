﻿using AutoMapper;
using FPSC.Data.DTO;
using FPSC.Data.Models;
using System;
using System.Linq;

namespace FPSC.MapperProfiles.Profiles
{
    public class PizzaProfile : Profile
    {
        public PizzaProfile()
        {
            CreateMap<PizzaMenuItem, PizzaMenuItemDTO>();
            CreateMap<PizzaOrderItem, PizzaUserOrderItemDTO>();
            CreateMap<PizzaOrder, PizzaOrderDTO>().ConvertUsing(PizzaOrderToDTO);    
        }
        
        private PizzaOrderDTO PizzaOrderToDTO(PizzaOrder pizzaOrder, PizzaOrderDTO dto, ResolutionContext context)
        {
            return new PizzaOrderDTO()
            {
                PizzaOrderId = pizzaOrder.PizzaOrderId,
                Date = pizzaOrder.Date,
                UserOrders = pizzaOrder.PizzaOrderItems.GroupBy(item => item.UserName).Select(grp => {
                    var name = grp.First().UserName;
                    return new PizzaUserOrderDTO()
                    {
                        UserName = name,
                        OrderItems = grp.Select(context.Mapper.Map<PizzaUserOrderItemDTO>).ToList()
                    };
                }).ToList()
            };
        }

    }
}
