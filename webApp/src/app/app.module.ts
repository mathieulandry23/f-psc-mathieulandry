import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";


import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { PizzaService } from "./services/pizza.service";
import { OrderComponent } from './components/order/order.component';
import { HomeComponent } from './components/home/home.component';
import { PizzaComponent } from './components/pizza/pizza.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'order/:id', component: OrderComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    OrderComponent,
    HomeComponent,
    PizzaComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    PizzaService
  ],
  bootstrap: [AppComponent]
})


export class AppModule { }
