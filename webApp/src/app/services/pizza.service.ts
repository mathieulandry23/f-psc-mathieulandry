import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { Pizza } from "../models/pizza";

@Injectable()
export class PizzaService {

  constructor(private http:Http) { }


  public GetMenu():Promise<Pizza[]> {
    return this.http.get("http://localhost:63353/api/Pizza/menu").toPromise().then(resp=> resp.json().map((data)=>{return <Pizza> data}));
  }

  public Purchase(pizza:Pizza, count:number){

    
    var orderIds:Array<any> = new Array<any>();
    for(var i = 0; i < count; i++){
      orderIds.push({"PizzaMenuItemId": pizza.pizzaMenuItemId})
    }
    
    this.http.post("http://localhost:63353/api/Pizza/order", {
      "userName" : "anonymous",
      "orderItems" : orderIds
    }).toPromise().then(r=>console.log(r)).catch(err=>console.log(err));
  }

}
