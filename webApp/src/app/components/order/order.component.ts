import { Component, OnInit } from '@angular/core';
import { Pizza } from "../../models/pizza";
import { ActivatedRoute } from "@angular/router";
import { PizzaService } from "../../services/pizza.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  public pizza:Pizza;
  private pizzaId:number;

  public OrderNumber:number;

  constructor( private route: ActivatedRoute, public pizzaService:PizzaService) {
    this.OrderNumber = 1;
    this.route.params.subscribe( params => this.pizzaId = params["id"] );
    
    pizzaService.GetMenu().then(
      r=>{
        this.pizza = r.find(x=>x.pizzaMenuItemId == this.pizzaId);
      }
    );

    
    
  }

  public Order(pizza:Pizza, count:number){
    this.pizzaService.Purchase(pizza, count);
  }

  ngOnInit() {
  }
}
