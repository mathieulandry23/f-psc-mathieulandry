import { Component, OnInit } from '@angular/core';
import { PizzaService } from "../../services/pizza.service";
import {Pizza } from "../../models/pizza";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public Pizzas:Pizza[];

  constructor(private pizzaService:PizzaService) { }

  ngOnInit() {
    this.pizzaService.GetMenu().then(r=>{
      this.Pizzas = r;
    });
  }

}
