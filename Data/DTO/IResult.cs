﻿using System.Collections.Generic;

namespace FPSC.Data.DTO
{
    public interface IResult
    {
        List<string> Messages { get; }
        bool Success { get; }
    }

    public interface IResult<T>: IResult
    {
        T Data { get; set; }
        void Deconstruct(out T data, out List<string> messages);
    }
}