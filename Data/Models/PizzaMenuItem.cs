﻿namespace FPSC.Data.Models
{
    public class PizzaMenuItem
    {
        public int PizzaMenuItemId { get; set; }
        public int PizzaTypeId { get; set; }
        public int PizzaSizeId { get; set; }
        public decimal Price { get; set; }

        public PizzaSize PizzaSize { get; set; }
        public PizzaType PizzaType { get; set; }
    }
}
