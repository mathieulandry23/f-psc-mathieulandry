namespace FPSC.Data.Models
{
    public class PizzaOrderItem
    {
        public int PizzaOrderItemId { get; set; }
        public string UserName { get; set; }

        public int PizzaTypeId { get; set; }
        public PizzaType PizzaType { get; set; }

        public int PizzaSizeId { get; set; }
        public PizzaSize PizzaSize { get; set; }

        public int PizzaOrderId { get; set; }
        public PizzaOrder PizzaOrder { get; set; }
    }
}