using System;
using System.Collections.Generic;

namespace FPSC.Data.Models
{
    public class PizzaOrder
    {
        public int PizzaOrderId { get; set; }
        public DateTime Date { get; set; }
        


        public ICollection<PizzaOrderItem> PizzaOrderItems { get; set; }
    }
}